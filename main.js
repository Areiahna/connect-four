/*
1. Initialize Game
    - display the board
    - display instructions
    - display whose turn it is
    - set up click handlers for columns
2. Take player input
    - Determine which column was clicked
    - Determine which color was dropped
    - After the drop, switch active player
3. Check for endGame conditions
    - See if there is 4-in-a-row horz, vert, diag
    - See if the game is a tie
    - Show a game end message
    - Allow the players to start over
*/
const boardModel = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],


];



const displayBoard = (function(boardModel, colIndex) {
    //create & display column div
    let colDiv = document.createElement("div")
    colDiv.classList.add("column")
  colDiv.addEventListener("click", dropDisk)


    for (let rowIndex = 0; rowIndex <=5; rowIndex++) {
        const div = document.createElement("div")
        div.classList.add("cell")
        div.dataset.row = rowIndex
        div.dataset.column = colIndex


        document.getElementById("gameBoard").appendChild(div)
        colDiv.appendChild(div)

    }
    document.getElementById("gameBoard").appendChild(colDiv)
})

boardModel[0].forEach(displayBoard)

// Adding disc to cell
let currentPlayer = 1

let play = true

function dropDisk(event) {
if (play === true){

    let currentColCells = event.currentTarget.children
    let currentCell = 0
    let currentDisc = document.createElement("div")
    currentDisc.classList.add("currentDisc")

    for (let x = 5; x >= 0; x--) {
        currentCell = currentColCells[x]
        const currentCol = currentCell.dataset.column
        const currentRow = currentCell.dataset.row
        if (currentCell.childElementCount === 0 && currentPlayer === 1) {
            currentCell.appendChild(currentDisc)
            boardModel[currentRow][currentCol] = 1
                // boardModel[x].splice(currentRow, 0, currentDisc)
           
            // console.log("Column: " + currentCol)
            // console.log("Row: " + currentRow)
            // console.log(boardModel)
            break
        }
        else{ if (currentCell.childElementCount === 0 && currentPlayer === 2) {
            currentCell.appendChild(currentDisc)
            boardModel[currentRow][currentCol] = 2
                // boardModel[x].splice(currentRow, 0, currentDisc)
           
            // console.log("Column: " + currentCol)
            // console.log("Row: " + currentRow)
            // console.log(boardModel)
            break
        }

        }
    }

    if (currentPlayer === 1) {
        currentDisc.style.backgroundColor = "red"
        currentPlayer = 2
    } else {
        currentDisc.style.backgroundColor = "black"
        currentPlayer = 1
    }

checkForWin()

 }
}

 //Checking Winning Combinations

function checkForWin() {
   
    //horizontal
    for (let i = 1; i <= 2; i++) {
        for (let col = 0; col <= 3; col++) {
            for (let row = 0; row <= 5; row++) {
                if (boardModel[row][col] == i) {
                    if ((boardModel[row][col + 1] == i) && (boardModel[row][col + 2] == i) && (boardModel[row][col + 3] == i)) {
                      endGame(i);
                        
                    }
                }
            }
        }
    }

    //vertical
    for (i = 1; i <= 2; i++) {
        for (let col = 0; col <= 6; col++) {
            for (let row = 0; row <= 2; row++) {
                if (boardModel[row][col] == i) {
                    if ((boardModel[row + 1][col] == i) && (boardModel[row + 2][col] == i) && (boardModel[row + 3][col] == i)) {
                        endGame(i);
                        return true;
                    }
                }
            }
        }
    }
    for (i = 1; i <= 2; i++) {
        for (let col = 0; col <= 3; col++) {
            for (let row = 0; row <= 2; row++) {
                if (boardModel[row][col] == i) {
                    if ((boardModel[row + 1][col + 1] == i) && (boardModel[row + 2][col + 2] == i) && (boardModel[row + 3][col + 3] == i)) {
                        endGame(i);
                        return true;
                    }
                }
            }
        }
    }

   //diagnol
   for (i = 1; i <= 2; i++) {
    for (let col = 0; col <= 3; col++) {
        for (let row = 3; row <= 5; row++) {
            if (boardModel[row][col] == i) {
                if ((boardModel[row - 1][col + 1] == i) && (boardModel[row - 2][col + 2] == i) && (boardModel[row - 3][col + 3] == i)) {
                    endGame(i);
                    return true;
                }
            }
        }
    }
}
}


function endGame(){
    

    if (currentPlayer === 1){

       document.getElementById(winBox)
       winBox.classList.add("winner")
       let outPut = document.createTextNode("Player Two Wins!")
       winBox.appendChild(outPut)
     
    } else {

      document.createElement("winBox")
       winBox.classList.add("winner")
       let outPut = document.createTextNode("Player One Wins!")
       winBox.appendChild(outPut)
        
    }
    play = false
    }
